package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"strconv"
	"sync"
)

const (
	defaultPort               = 8080
	defaultApiKey             = "DEMO_KEY"
	defaultConcurrentRequests = 5
)

var loadOnce sync.Once

type Config struct {
	ApiKey             string
	Port               int
	ConcurrentRequests int
}

var config Config

func Load() Config {
	loadOnce.Do(load)
	return config
}

func load() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Couldn't locate .env file. Using provided ENVs or defaults")
	}

	config = Config{}

	if host := os.Getenv("API_KEY"); host != "" {
		config.ApiKey = host
	} else {
		config.ApiKey = defaultApiKey
	}

	if crq := os.Getenv("CONCURRENT_REQUESTS"); crq != "" {
		config.ConcurrentRequests, _ = strconv.Atoi(crq)
	} else {
		config.ConcurrentRequests = defaultConcurrentRequests
	}

	if port := os.Getenv("PORT"); port != "" {
		config.Port, _ = strconv.Atoi(port)
	} else {
		config.Port = defaultPort
	}
}
