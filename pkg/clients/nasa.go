package clients

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/k980/go-url-collector/pkg/config"
	"gitlab.com/k980/go-url-collector/pkg/date"
	"gitlab.com/k980/go-url-collector/pkg/workerpool"
	"golang.org/x/sync/semaphore"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	baseUrl = "https://api.nasa.gov"
)

type ApodData struct {
	URL string `json:"url"`
}

type apodClient struct {
	client http.Client
	cfg    config.Config
	sem    *semaphore.Weighted
}

func NewNasaClient(client http.Client, cfg config.Config) *apodClient {
	return &apodClient{
		client: client,
		cfg:    cfg,
		sem:    semaphore.NewWeighted(int64(cfg.ConcurrentRequests)),
	}
}

func (a *apodClient) GetUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error) {
	var tasks []workerpool.Task

	for d := startDate; !d.After(endDate); d = d.AddDate(0, 0, 1) {
		tasks = append(tasks, workerpool.Task{
			Exec: func(args interface{}) (interface{}, error) {
				if err := a.sem.Acquire(ctx, 1); err != nil {
					return nil, err
				}
				defer a.sem.Release(1)

				apod, err := a.getImageUrl(args.(string))
				if err != nil {
					return nil, err
				}
				return apod.URL, nil
			},
			Args: d.Format(date.TimeLayout),
		})
	}

	wp := workerpool.New(a.cfg.ConcurrentRequests)
	go wp.AddTasks(tasks)
	go wp.Run(ctx)

	return a.collectResults(wp)
}

func (a *apodClient) getImageUrl(date string) (ApodData, error) {
	url := fmt.Sprintf("%s/planetary/apod?api_key=%s&date=%s", baseUrl, a.cfg.ApiKey, date)
	res, err := a.client.Get(url)
	if err != nil {
		return ApodData{}, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return ApodData{}, err
	}

	var apod ApodData
	err = json.Unmarshal(body, &apod)
	if err != nil {
		return ApodData{}, err
	}
	return apod, nil
}

func (a *apodClient) collectResults(wp workerpool.WorkerPool) (urls []string, err error) {
	for {
		select {
		case res, ok := <-wp.Results():
			if !ok {
				continue
			}
			if res.Err != nil {
				return nil, res.Err
			}
			urls = append(urls, res.Value.(string))
		case <-wp.Done():
			return urls, nil
		}
	}
}
