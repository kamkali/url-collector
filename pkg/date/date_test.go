package date

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"testing"
	"time"
)

var _ = Describe("Less than or equal date", func() {
	Context("when start date is less than endDate", func() {
		It("returns true", func() {
			startDate, _ := time.Parse(TimeLayout, "1920-01-01")
			endDate, _ := time.Parse(TimeLayout, "2021-01-01")
			Expect(LessThanEqual(startDate, endDate)).To(BeTrue())
		})
	})

	Context("when start date is larger than endDate", func() {
		It("returns false", func() {
			startDate, _ := time.Parse(TimeLayout, "2021-01-01")
			endDate, _ := time.Parse(TimeLayout, "1920-01-01")
			Expect(LessThanEqual(startDate, endDate)).To(BeFalse())
		})
	})

	Context("when start date is equal endDate", func() {
		It("returns true", func() {
			startDate, _ := time.Parse(TimeLayout, "2021-10-19")
			endDate, _ := time.Parse(TimeLayout, "2021-10-19")
			Expect(LessThanEqual(startDate, endDate)).To(BeTrue())
		})
	})
})

var _ = Describe("Validate date range", func() {
	Context("when end date Before start date", func() {
		It("returns false", func() {
			startDate, _ := time.Parse(TimeLayout, "2021-01-01")
			endDate, _ := time.Parse(TimeLayout, "1920-01-01")
			Expect(ValidDateRange(startDate, endDate)).To(BeFalse())
		})
	})

	Context("when end date after now", func() {
		It("returns false", func() {
			startDate, _ := time.Parse(TimeLayout, "2021-01-01")
			endDate, _ := time.Parse(TimeLayout, "9999-01-01")
			Expect(ValidDateRange(startDate, endDate)).To(BeFalse())
		})
	})

	Context("when start date after now", func() {
		It("returns false", func() {
			startDate, _ := time.Parse(TimeLayout, "9999-01-01")
			endDate, _ := time.Parse(TimeLayout, "9999-01-01")
			Expect(ValidDateRange(startDate, endDate)).To(BeFalse())
		})
	})
})

func TestSuite(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "date package")
}
