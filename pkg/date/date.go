package date

import (
	"time"
)

const (
	TimeLayout = "2006-01-02"
)

func ValidDateRange(startDate time.Time, endDate time.Time) bool {
	return LessThanEqual(startDate, time.Now()) && LessThanEqual(endDate, time.Now()) && LessThanEqual(startDate, endDate)
}

func LessThanEqual(t1 time.Time, t2 time.Time) bool {
	return t1.Before(t2) || t1.Equal(t2)
}
