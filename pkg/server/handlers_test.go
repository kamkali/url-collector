package server

import (
	"context"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/k980/go-url-collector/pkg/config"
	"gitlab.com/k980/go-url-collector/pkg/services"
	"net/http"
	"net/http/httptest"
	"time"
)

var _ = Describe("GetPictureUrls", func() {
	gin.SetMode(gin.TestMode)

	var (
		s           *server
		c           *gin.Context
		cfg         config.Config
		w           *httptest.ResponseRecorder
		mockService services.UrlCollector
	)

	Context("query string", func() {
		BeforeEach(func() {
			cfg = config.Load()
			mockService = &urlCollectorServiceMock{}
			w = httptest.NewRecorder()
			c, _ = gin.CreateTestContext(w)
			r := gin.Default()
			s = New(cfg, r, mockService)
		})

		When("received valid 'from' and 'to' query string", func() {
			BeforeEach(func() {
				setMockHttpRequest(c, "/pictures")
				c.Set("from", time.Now())
				c.Set("to", time.Now())
				s.GetPictureUrls()(c)
			})

			It("returns status code 200", func() {
				Expect(w.Code).To(Equal(http.StatusOK))
			})
			It("returns image url", func() {
				Expect(w.Body).Should(MatchJSON(`{"urls":["image1.jpg"]}`))
			})
		})
	})

	Context("service fails", func() {
		BeforeEach(func() {
			cfg = config.Load()
			mockService = &urlCollectorServiceErrorMock{}
			w = httptest.NewRecorder()
			c, _ = gin.CreateTestContext(w)
			s = New(cfg, gin.Default(), mockService)
			setMockHttpRequest(c, "/pictures")
			c.Set("from", time.Now())
			c.Set("to", time.Now())
			s.GetPictureUrls()(c)
		})

		It("returns status code 500", func() {
			Expect(w.Code).To(Equal(http.StatusInternalServerError))
		})
		It("returns err", func() {
			Expect(w.Body).To(MatchJSON(fmt.Sprintf(`{"error":"%s"}`, serviceFailed)))
		})
	})
})

var _ = Describe("Health endpoint", func() {
	gin.SetMode(gin.TestMode)

	var (
		s           *server
		c           *gin.Context
		cfg         config.Config
		w           *httptest.ResponseRecorder
		mockService services.UrlCollector
	)

	Context("when app ready", func() {
		BeforeEach(func() {
			cfg = config.Load()
			mockService = &urlCollectorServiceMock{}
			w = httptest.NewRecorder()
			c, _ = gin.CreateTestContext(w)
			r := gin.Default()
			s = New(cfg, r, mockService)
		})

		It("returns status code 200", func() {
			s.HealthCheck()(c)
			Expect(w.Code).To(Equal(http.StatusOK))
		})
	})
})

func setMockHttpRequest(c *gin.Context, path string) {
	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		Expect(err).ToNot(HaveOccurred())
	}
	c.Request = req
}

type urlCollectorServiceMock struct{}
type urlCollectorServiceErrorMock struct{}

func (u *urlCollectorServiceMock) FetchUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error) {
	return []string{"image1.jpg"}, nil
}

func (u *urlCollectorServiceErrorMock) FetchUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error) {
	return nil, errors.New("err")
}
