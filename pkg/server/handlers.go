package server

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

const (
	invalidDateRange  = "Specified time range is invalid."
	invalidDateFormat = "Specified time format is invalid. Must be in yyyy-MM-dd format."
	serviceFailed     = "Could not fetch the relevant data."
)

type ErrorResponse struct {
	Message string `json:"error"`
}

type UrlsResponse struct {
	Urls []string `json:"urls"`
}

func (s *server) GetPictureUrls() gin.HandlerFunc {
	return func(c *gin.Context) {
		startDate := c.MustGet("from").(time.Time)
		endDate := c.MustGet("to").(time.Time)

		urls, err := s.urlCollector.FetchUrlsFromDateRange(c, startDate, endDate)
		if err != nil {
			c.JSON(http.StatusInternalServerError, ErrorResponse{Message: serviceFailed})
			return
		}

		c.JSON(http.StatusOK, UrlsResponse{Urls: urls})
	}
}

func (s *server) HealthCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(200, "Ok")
	}
}
