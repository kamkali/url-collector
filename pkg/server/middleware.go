package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/k980/go-url-collector/pkg/date"
	"net/http"
	"time"
)

func (s *server) ValidateDateRange() gin.HandlerFunc {
	return func(c *gin.Context) {
		if missingFromQueryWhenToExists(c) {
			c.AbortWithStatusJSON(http.StatusUnprocessableEntity, ErrorResponse{Message: invalidDateRange + " Missing 'from' query parameter when using 'to'"})
			return
		}
		startDate, err := getDateFromQueryString(c, "from")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnprocessableEntity, ErrorResponse{Message: invalidDateFormat + " Query string: 'from'"})
			return
		}
		endDate, err := getDateFromQueryString(c, "to")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnprocessableEntity, ErrorResponse{Message: invalidDateFormat + " Query string: 'to'"})
			return
		}

		if !date.ValidDateRange(startDate, endDate) {
			c.AbortWithStatusJSON(http.StatusUnprocessableEntity, ErrorResponse{Message: invalidDateRange})
			return
		}
		c.Set("from", startDate)
		c.Set("to", endDate)
		c.Next()
	}
}

func getDateFromQueryString(c *gin.Context, key string) (d time.Time, err error) {
	if val := c.Query(key); val == "" {
		d = time.Now().Truncate(time.Hour)
		d = time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, 0, time.Local)
	} else {
		d, err = time.Parse(date.TimeLayout, val)
	}
	return
}

func missingFromQueryWhenToExists(c *gin.Context) bool {
	return c.Query("from") == "" && c.Query("to") != ""
}
