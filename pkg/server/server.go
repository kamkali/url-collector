package server

import (
	"context"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/k980/go-url-collector/pkg/config"
	"gitlab.com/k980/go-url-collector/pkg/services"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type server struct {
	router       *gin.Engine
	urlCollector services.UrlCollector
	config       config.Config
}

func New(config config.Config, router *gin.Engine, urlCollector services.UrlCollector) *server {
	return &server{
		router:       router,
		urlCollector: urlCollector,
		config:       config,
	}
}

func (s *server) Run() {
	s.registerRoutes()
	srv := http.Server{
		Addr:    ":" + fmt.Sprintf("%d", s.config.Port),
		Handler: s.router,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("server listen errored: %s\n", err)
		}
	}()
	log.Printf("server listening on port: %d", s.config.Port)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}
	log.Println("Server exiting")
}
