package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/k980/go-url-collector/pkg/config"
	"gitlab.com/k980/go-url-collector/pkg/services"
	"net/http"
	"net/http/httptest"
	"time"
)

var _ = Describe("ValidateDateRange middleware", func() {
	gin.SetMode(gin.TestMode)

	var (
		s           *server
		c           *gin.Context
		cfg         config.Config
		w           *httptest.ResponseRecorder
		mockService services.UrlCollector
		today       time.Time
	)

	Context("query string", func() {
		BeforeEach(func() {
			cfg = config.Load()
			mockService = &urlCollectorServiceMock{}
			w = httptest.NewRecorder()
			c, _ = gin.CreateTestContext(w)
			s = New(cfg, gin.Default(), mockService)
		})

		When("not received or empty", func() {
			BeforeEach(func() {
				s.ValidateDateRange()(c)
				d := time.Now()
				today = time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, 0, time.Local)
			})

			It("sets 'from' to current date", func() {
				Expect(c.MustGet("from")).To(Equal(today))
			})
			It("sets 'to' to current date", func() {
				Expect(c.MustGet("to")).To(Equal(today))
			})
		})

		When("received valid, single 'to' query string", func() {
			BeforeEach(func() {
				setMockHttpRequest(c, "/pictures")
				c.Request.URL.RawQuery = "to=2021-10-01"
				s.ValidateDateRange()(c)
			})

			It("returns status code 422", func() {
				Expect(w.Code).To(Equal(http.StatusUnprocessableEntity))
			})
			It("returns err", func() {
				Expect(w.Body).To(MatchJSON(fmt.Sprintf(`{"error":"%s"}`, invalidDateRange+" Missing 'from' query parameter when using 'to'")))
			})
		})

		When("received invalid 'from' query string", func() {
			BeforeEach(func() {
				setMockHttpRequest(c, "/pictures")
				c.Request.URL.RawQuery = "from=2010-01"
				s.ValidateDateRange()(c)
			})

			It("returns status code 422", func() {
				Expect(w.Code).To(Equal(http.StatusUnprocessableEntity))
			})
			It("returns err", func() {
				Expect(w.Body).To(MatchJSON(fmt.Sprintf(`{"error":"%s"}`, invalidDateFormat+" Query string: 'from'")))
			})
		})

		When("received invalid 'to' query string", func() {
			BeforeEach(func() {
				setMockHttpRequest(c, "/pictures")
				c.Request.URL.RawQuery = "from=2021-10-10&to=2010-01"
				s.ValidateDateRange()(c)
			})

			It("returns status code 422", func() {
				Expect(w.Code).To(Equal(http.StatusUnprocessableEntity))
			})
			It("returns err", func() {
				Expect(w.Body).To(MatchJSON(fmt.Sprintf(`{"error":"%s"}`, invalidDateFormat+" Query string: 'to'")))
			})
		})

		When("received invalid date range", func() {
			BeforeEach(func() {
				setMockHttpRequest(c, "/pictures")
				c.Request.URL.RawQuery = "from=2023-10-10&to=2010-05-01"
				s.ValidateDateRange()(c)
			})

			It("returns status code 422", func() {
				Expect(w.Code).To(Equal(http.StatusUnprocessableEntity))
			})
			It("returns err", func() {
				Expect(w.Body).To(MatchJSON(fmt.Sprintf(`{"error":"%s"}`, invalidDateRange)))
			})
		})
	})
})
