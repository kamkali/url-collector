package server

import (
	helmet "github.com/danielkov/gin-helmet"
	"github.com/gin-contrib/cors"
)

func (s *server) registerRoutes() {
	s.router.GET("/health", s.HealthCheck())

	s.router.Use(s.ValidateDateRange())
	s.router.Use(helmet.Default())
	s.router.Use(cors.Default())
	s.router.GET("/pictures", s.GetPictureUrls())
}
