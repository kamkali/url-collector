package workerpool

type ExecFn func(args interface{}) (interface{}, error)

type Task struct {
	Exec ExecFn
	Args interface{}
}

type Result struct {
	Value interface{}
	Err   error
}

func (t Task) execute() Result {
	val, err := t.Exec(t.Args)
	if err != nil {
		return Result{Err: err}
	}
	return Result{Value: val}
}
