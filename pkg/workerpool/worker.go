package workerpool

import (
	"context"
	"sync"
)

type WorkerPool interface {
	Run(ctx context.Context)
	AddTasks([]Task)
	Done() <-chan done
	Results() <-chan Result
}

type workerPool struct {
	maxWorker int
	taskCh    chan Task
	resultCh  chan Result
	done      chan done
}

type done struct{}

func New(workers int) WorkerPool {
	return &workerPool{
		maxWorker: workers,
		taskCh:    make(chan Task, workers),
		resultCh:  make(chan Result, workers),
		done:      make(chan done),
	}
}

func worker(ctx context.Context, wg *sync.WaitGroup, tasks <-chan Task, results chan<- Result) {
	defer wg.Done()
	for {
		select {
		case t, ok := <-tasks:
			if !ok {
				return
			}
			results <- t.execute()
		case <-ctx.Done():
			results <- Result{
				Err: ctx.Err(),
			}
		}

	}
}

func (wp *workerPool) Run(ctx context.Context) {
	var wg sync.WaitGroup
	for i := 1; i <= wp.maxWorker; i++ {
		wg.Add(1)
		go worker(ctx, &wg, wp.taskCh, wp.resultCh)
	}
	wg.Wait()
	close(wp.resultCh)
	close(wp.done)
}

func (wp *workerPool) AddTasks(tasks []Task) {
	for _, t := range tasks {
		wp.taskCh <- t
	}
	close(wp.taskCh)
}

func (wp *workerPool) Done() <-chan done {
	return wp.done
}

func (wp *workerPool) Results() <-chan Result {
	return wp.resultCh
}
