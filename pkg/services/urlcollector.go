package services

import (
	"context"
	"time"
)

type UrlCollector interface {
	FetchUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error)
}

type urlCollector struct {
	urlClient UrlClient
}

func NewUrlCollector(client UrlClient) *urlCollector {
	return &urlCollector{client}
}

func (u *urlCollector) FetchUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error) {
	urls, err := u.urlClient.GetUrlsFromDateRange(ctx, startDate, endDate)
	if err != nil {
		return nil, err
	}
	return urls, nil
}
