package services_test

import (
	"context"
	"errors"
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/k980/go-url-collector/pkg/date"
	"gitlab.com/k980/go-url-collector/pkg/services"
	"testing"
	"time"
)

var _ = Describe("GetPictureUrls", func() {
	var (
		svc services.UrlCollector
		ctx context.Context
	)

	Context("when valid date range", func() {
		BeforeEach(func() {
			nm := &NasaMock{}
			service := services.NewUrlCollector(nm)
			svc = service
			ctx = context.Background()
		})

		It("returns single url when dates are equal", func() {
			dateRange, _ := svc.FetchUrlsFromDateRange(ctx, newDate(2020, 10, 20), newDate(2020, 10, 20))
			Expect(len(dateRange)).To(Equal(1))
		})
		It("returns urls list", func() {
			dateRange, _ := svc.FetchUrlsFromDateRange(ctx, newDate(2020, 10, 20), newDate(2020, 10, 22))
			Expect(len(dateRange)).To(Equal(3))
		})
		It("doesn't return errors", func() {
			_, err := svc.FetchUrlsFromDateRange(ctx, newDate(2020, 10, 20), newDate(2020, 10, 22))
			Expect(err).NotTo(HaveOccurred())
		})
	})

	Context("when client errored", func() {
		BeforeEach(func() {
			nm := &NasaErrorMock{}
			service := services.NewUrlCollector(nm)
			svc = service
			ctx = context.Background()
		})

		It("returns error", func() {
			_, err := svc.FetchUrlsFromDateRange(ctx, time.Time{}, time.Time{})
			Expect(err).To(HaveOccurred())
		})
	})
})

type NasaMock struct{}
type NasaErrorMock struct{}

func (n *NasaMock) GetUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error) {
	var urls []string

	i := 0
	for d := startDate; d.After(endDate) == false; d = d.AddDate(0, 0, 1) {
		i++
		jpg := fmt.Sprintf("SpaceCat_%d", i)
		urls = append(urls, jpg)
	}
	return urls, nil
}

func (n *NasaErrorMock) GetUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error) {
	return nil, errors.New("something went wrong")
}

func newDate(year, month, day int) time.Time {
	d, _ := time.Parse(date.TimeLayout, fmt.Sprintf("%d-%d-%d", year, month, day))
	return d
}

func TestSuite(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Url Collector Service")
}
