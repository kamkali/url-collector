package services

import (
	"context"
	"time"
)

type UrlClient interface {
	GetUrlsFromDateRange(ctx context.Context, startDate time.Time, endDate time.Time) ([]string, error)
}
