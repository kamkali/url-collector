package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/k980/go-url-collector/pkg/clients"
	"gitlab.com/k980/go-url-collector/pkg/config"
	"gitlab.com/k980/go-url-collector/pkg/server"
	"gitlab.com/k980/go-url-collector/pkg/services"
	"net/http"
)

func main() {
	cfg := config.Load()
	httpClient := http.Client{}
	nasaClient := clients.NewNasaClient(httpClient, cfg)
	urlService := services.NewUrlCollector(nasaClient)
	r := gin.Default()
	s := server.New(cfg, r, urlService)
	s.Run()
}
