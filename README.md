# Url collector

[GoGoApps](https://gogoapps.io/) recruitment task solution for url-collector — a microservice that would prepare a list of urls for the another microservices to use.

## Table of contents
* [Assumptions](#assumptions)
* [Solution](#solution)
* [Running the project](#running-the-project)
  * [Environment variables](#environment-variables)
  * [Make commands](#make-commands)
* [Usage](#usage)
* [Built with](#built-with)

## Assumptions

1. When not `from` nor `to` specified, use today's date as default
2. Can use single `from` query string, `to` assumed for today's date
3. Can't use single `to` query string — returns error
4. No more than `CONCURRENT_REQUESTS` can hit single external api
5. Single NASA API client
6. Beared in mind possibility to extend with other clients in the future

## Solution

The app utilize clean architecture approach.
Decoupled dependency structure makes it extensible and testable at every stage.

Docker is using multi-stage building approach for optimized final app container (~7MB).

To limit concurrent requests, the Workerpool concurrency pattern is used.
Each instance spins up to `CONCURRENT_REQUESTS` worker goroutines.
To ensure the limit is not exceeded with concurrent http server, the number of goroutines per client implementation (nasa) is controlled via semaphore.
> Rationale:
It makes sense to make Workerpool a package and control the total number per its instance.
If we were to implement another API client, Workerpool package can be utilized to spin more goroutines to another client.



## Running the project
Locally:
- Make sure you have GoLang installed. [Get GoLang](https://golang.org/doc/install)
- Copy `.env.example` to `.env` and fill your values.
  - Omit this step for default values [listed here](#environment-values)
- Run `make run` to run from the source code

With Docker
- Make sure you have docker installed. [Get Docker](https://docs.docker.com/get-docker/)
- Run `make run_from_registry` to start app from Gitlab Container Registry
- To build locally:
  - Copy `.env.example` to `.env` and fill your values.
    - Omit this step for default values [listed here](#environment-values)
  - `make run_image` to build and run app from image




#### Environment variables
<details>

| Key                     | Default value            | Desc                                    |
| ----------------------- | ------------------------ | --------------------------------------- |
| `API_KEY`               | `DEMO_KEY`               | NASA API key                            |
| `CONCURRENT_REQUESTS`   | `5`                      | Max concurrent requests to external API |
| `PORT`                  | `8080`                   | Default port at which app runs          |

</details>

#### Make commands

<details>

| Command                  | Desc                                                   |
| ------------------------ | ------------------------------------------------------ |
| `make test`              | run unit tests                                         |
| `make check`             | tidy dependencies, lint, fmt and test                  |
| `make run`               | run the app from source code                           |
| `make build_image`       | build Docker image  locally                            |
| `make run_image`         | build and run image locally                            |
| `make run_from_registry` | run released version from Gitlab Container Registry    |

</details>

## Usage

* Return single image url from today
> Request:
```bash
curl http://localhost:8080/pictures
```
Response:
```
{
  "urls": [
      "https://apod.nasa.gov/apod/image/2110/ana03BennuVantuyne1024c.jpg"
    ]
}
```

* With single `from` return image urls from given `from` date to assumed  `to` equal today:
> Request:
```bash
curl http://localhost:8080/pictures?from=2021-10-21
```
Response:
```
{
  "urls": [
      "https://apod.nasa.gov/apod/image/2110/SH2-308NS_1024.jpg",
      "https://apod.nasa.gov/apod/image/2110/67p_m1_vdb47_1024.jpg"
    ]
}
```

* With specified time range `from` date up to `to` date:
> Request:
```bash
curl http://localhost:8080/pictures?from=2021-10-01&to=2021-10-09
```
Response:
```
  {
    "urls": [
        "https://apod.nasa.gov/apod/image/2110/ArcsOfLightAndDust1024.jpg",
        "https://apod.nasa.gov/apod/image/2110/M8-Pipe_APOD_GabrielSantosSmall.jpg",
        "https://apod.nasa.gov/apod/image/2110/teapotsirds_winfree_960.jpg",
        "https://apod.nasa.gov/apod/image/2110/SouthPoleSunRise_Wolf_960.jpg",
        "https://apod.nasa.gov/apod/image/2110/NGC4676_HubbleOstling_960.jpg",
        "https://apod.nasa.gov/apod/image/2110/OrionStreams_Saukkonen_960.jpg",
        "https://apod.nasa.gov/apod/image/2110/NGC6559Sartori1024.jpg",
        "https://apod.nasa.gov/apod/image/2110/doubleclustergroves1024.jpg",
        "https://apod.nasa.gov/apod/image/2110/peg51_desmars_ex1024.jpg"
      ]
  }
```


## Built with

* [GoLang](https://golang.org/) (obviously) — implementation language
* [Gin](https://github.com/gin-gonic/gin) — Web framework for Go
* [Ginkgo](https://github.com/onsi/ginkgo) / [Gomega](https://github.com/onsi/gomega)  — BDD Testing Framework for Go and its go-to matcher library
