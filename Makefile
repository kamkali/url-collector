test:
	go test -race -v ./...
check:
	go mod tidy
	golangci-lint run ./...
	go fmt ./...
	make test
run:
	go run cmd/main.go
build_image:
	make check
	docker build -t registry.gitlab.com/kamkali/url-collector .
run_image:
	make build_image
	docker run -p 8080:8080 registry.gitlab.com/kamkali/url-collector:latest
push_image:
	make build_image
	docker tag url-collector:latest registry.gitlab.com/kamkali/url-collector:latest && docker push registry.gitlab.com/kamkali/url-collector
run_from_registry:
	docker run -p 8080:8080 registry.gitlab.com/kamkali/url-collector:latest

.PHONY: test check run build_image run_image push_image run_from_registry