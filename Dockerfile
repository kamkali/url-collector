FROM golang:1.17-alpine AS builder

RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:x:65534:' > /user/group

RUN apk add --no-cache ca-certificates git

WORKDIR /app

COPY ./go.mod ./go.sum ./

COPY ./ ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
    -ldflags='-s -w' \
    -installsuffix 'static' \
    -o /app_main ./cmd/main.go

FROM scratch AS final

ENV GIN_MODE=release

COPY --from=builder /user/group /user/passwd /etc/
COPY --from=builder /app/.env /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app_main /app

EXPOSE 8080

USER nobody:nobody

ENTRYPOINT ["/app"]